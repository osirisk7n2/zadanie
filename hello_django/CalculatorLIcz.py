__author__ = 'ryujin'
import requests
from random import randint
import couchdb
import operator
from uuid import uuid4
import json
import httplib
from worker import Connection

couch=couchdb.Server('http://194.29.175.241:5984/')
db = couch['calc']


def ONP(server,expression,AvlOperations):
    result ="";
    ops = {'+': operator.add, '-': operator.sub, '*': operator.mul, '/': operator.div}
    st=[]
    for item in expression.split(' '):
        if item in ops:
            if(item not in AvlOperations):
                return ""
            y,x = st.pop(),st.pop()
            z = getResult(x,y,item,server)
            if z=="error":
                return "error 404"
            #z=ops[item](x,y)
            result=z
        else:
            z=float(item)
        st.append(z)
    return result



def getResult(liczba1,liczba2,operation,servers):
    if(str(operation)=="+"):
        return liczba1+liczba2
    AvailibleServers =[]
    licznik=0
    for serverz in servers:
        licnzik=licznik+1
        data=db[serverz]
        try:
            if data['operator']==str(operation):
                AvailibleServers.append(serverz)
        except:
            pass
        if(licznik>15):
            break

    HowManyAvailible= len(AvailibleServers)
    ktory =randint(0,HowManyAvailible)
    used = AvailibleServers[ktory-1]
    ddd=db[used]
    dataz = {'id':str(uuid4()),"number1":liczba1,"number2":liczba2}
    url=str(ddd['host'])+str(ddd['calculation'])
    try:
        r=requests.post(url,data=json.dumps(dataz))
        if(str(r.status_code)!='200'):
            return "Error"
    except:
        return "error 404"
    #data2 = json.loads(r.content)
    return r.content
