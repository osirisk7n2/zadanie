from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'hello_django.views.hello', name='hello'),
    # url(r'^hello_django/', include('hello_django.foo.urls')),
    url(r'^register/','hello_django.views.register',name='register'),
    url(r'^search/','hello_django.views.search',name='search'),
    url(r'^calculate/','hello_django.views.calculate',name='calculate'),
    url(r'^SendCalculate/','hello_django.views.SendCalculate',name="SendCalculate"),
    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
