__author__ = 'ryujin'
from django import forms
from django.contrib.auth.models import User

class LoginForm(forms.Form):
    login=forms.CharField(required=True)
    password=forms.CharField(widget=forms.PasswordInput,required=True)


class RegisterForm(forms.Form):
    login=forms.CharField(required=True)
    password=forms.CharField(widget=forms.PasswordInput,required=True)
    email=forms.EmailField(required=True)

class CalculateForm(forms.Form):
    obliczenie=forms.CharField(required=True)